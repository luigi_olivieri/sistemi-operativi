/* acrossProcessBoundary.c  
   4-17 Synchronization Across Process Boundaries
   a causa dell'uso di strerror_r, usato in printerror.h,
   compilare con -D_POSIX_C_SOURCE >= 200112L
*/

/* messo prima perche' contiene define _POSIX_C_SOURCE */
#include "printerror.h"
#include "DBGpthread.h"


#include <stdlib.h>
#include <unistd.h>   /* exit() etc */
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* Memory MANagement: shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
/*
#include <sys/ipc.h>
#include <sys/shm.h>
*/
#include <fcntl.h>
#include <sys/stat.h>

#define TURNO_PRODUTTORE 0
#define TURNO_CONSUMATORE 1

/*Contenitore della nostra memoria condivisa*/
typedef struct { 
     char buf; 		/* buffer di scambio */
     int occupied; 
     pthread_mutex_t mutex; 
     pthread_cond_t cond_full; 
     pthread_cond_t cond_empty; } 
buffer_t; 


void producer_driver(buffer_t *b);
void consumer_driver(buffer_t *b);

void producer(buffer_t *b, char item)
{
    /*Richiedo la mutua esclusione*/
    DBGpthread_mutex_lock(&b->mutex, "producer lock");
   
    /*Se è il turno del consumatore allora mi addormento*/
    while (b->occupied >= TURNO_CONSUMATORE)
        DBGpthread_cond_wait(&b->cond_empty, &b->mutex, "producer wait");

    /*Assert è una funzione di debuggin, se il valore che gli viene passato è false
     * allora chiama una exit() e da come codice di errore 3
     * Qui controlliamo che NON sia 0 il valore occupied
     * e che quindi non sia il turno del produttore perché se
     * così fosse ci sarebbe stato un qualche errore*/
    assert(b->occupied < 1);

    /*Carico sul buffer quello che è contenuto in item*/
    b->buf = item;
    /*Salvo che ora è il turno del consumatore*/
    b->occupied = 1;

    /*Stampo quello che ho messo sul buffer*/
	printf("producer puts %c\n", item );
	fflush(stdout);

    /*Avviso che il buffer è pieno e pronto per essere svuotato dal consumatore*/
    DBGpthread_cond_signal(&b->cond_full, "producer signal");
    /*Rilascio la mutua esclusione*/
    DBGpthread_mutex_unlock(&b->mutex, "producer unlock");
    /*Alla fine termina senza ritornare nulla!*/
}

char consumer(buffer_t *b)
{
    char item;

    /*Richiedo la mutua esclusione*/
    DBGpthread_mutex_lock(&b->mutex ,"consumer lock");
    /*Se è il turno del produttore mi metto in wait*/
    while(b->occupied <= TURNO_PRODUTTORE)
        DBGpthread_cond_wait(&b->cond_full, &b->mutex , "consumer wait");

    /*Assert è una funzione di debuggin, se il valore che gli viene passato è false
     * allora chiama una exit() e da come codice di errore 3
     * Qui controlliamo che NON sia 0 il valore occupied
     * e che quindi non sia il turno del produttore perché se
     * così fosse ci sarebbe stato un qualche errore*/
    assert(b->occupied > TURNO_PRODUTTORE);

    /*Prendo dal buffer il valore messo dal produttore*/
    item = b->buf;
    /*Imposto che è il turno del produttore*/
    b->occupied=TURNO_PRODUTTORE;

    /*Stampo quello che il consumatore ha preso dal buffer*/
	printf("consumer gets %c\n", item );
	fflush(stdout);

    /*Sveglio il produttore avvisando che il buffer deve essere riempito*/
    DBGpthread_cond_signal(&b->cond_empty, "consumer signal");
    /*Rilascio la mutua esclusione*/
    DBGpthread_mutex_unlock(&b->mutex, "consumer unlock");

    /*Termina la funzione ritornando il valore 'consumato'*/
    return(item);
}

/* il padre aspetta la terminazione del figlio */
void dezombizzaFiglio(pid_t pid) {
    pid_t retpid; int status;
    
    do {
        /*Quando entra qua dentro il pid sarà del padre o del figlio, quindi lanciamo
        * una waitpid così da attendere, il valore di ritorno verrà scritto in status, 
        * 0 è la modalità di attesa, per noi sarà sempre quello*/ 
        retpid = waitpid(pid, &status, 0);

    /*Controllo che sia andato tutto bene, se la waitpid ha dato problemi allora 
     * riavvio il ciclo altrimenti esco*/
    } while((retpid<0) && (errno==EINTR));
    /*Se dopo il ciclo non ci sono stati errori ma retpid è ancora negativo
     * ci deve essere qualcosa che blocca la wait pid e quindi lo stampo ed esco
     * nota: il figlio non dovrebbe essere ancora terminato*/
    if(retpid<0) {
        PrintErrnoAndExit ( "waitpid failed " );
        exit(3);
    }
    /*Invece se è andato tutto bene nella wait*/
    else {
        /*Controllo se lo stato del figlio è diverso da 0, in quel caso tutto è ok*/
        if(WIFEXITED(status)) { /* figlio terminato normalmente */
            int rc;
            /*Decodifico l'exit status in modo classico*/
            rc = WEXITSTATUS(status);
            printf("figlio restituisce status %d\n", rc );
            fflush(stdout);
        }
        /*Il figlio ha avuto dei problemi ma è comunque terminato*/
        else {
            printf("figlio termina in modo anormale\n" );
            fflush(stdout);
        }
    }
}


int main() {
    int shmfd, rc;
    pid_t pid;
    buffer_t *buffer;
    int shared_seg_size = sizeof(buffer_t);
    pthread_mutexattr_t mattr;
    pthread_condattr_t cvattr;

    /*Creiamo il file e metto il file descriptor dentro shmfd*/
    shmfd = shm_open( "/pedala", O_CREAT /*| O_EXCL*/ | O_RDWR, S_IRWXU );
    if (shmfd < 0) {
        perror("In shm_open()");
        exit(1);
    }

    /*Setto la dimensione dello spazio di memoria condivisa pari a quello della struttura*/
    /* adjusting mapped file size (make room for the whole segment to map) */
    rc = ftruncate(shmfd, shared_seg_size);
    if (rc != 0) {
        perror("ftruncate() failed");
        exit(1);
    }

    /*Mappo la memoria e ritorno un puntatore a quest'area che sarà salvato
     * dentro la struttura buffer*/
    buffer = (buffer_t *)mmap(NULL, sizeof(buffer_t),
        PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);
    /*Imposto il campo occupied della struttura a O dato che non è occupata*/
    buffer->occupied = 0;

    /*Inizializzo un attributo per la mutex chiamato mattr*/
    rc=pthread_mutexattr_init(&mattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_mutexattr_init  failed");
    /*Imposto l'attributo appena inizializzato per la condivisione della mutex*/
    rc=pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
    if( rc ) PrintERROR_andExit(rc,"pthread_mutexattr_setpshared  failed");
    /*Inizializzo una variabile mutex dentro la struttura buffer che ha 
     * come attributo la condivisione specificata dentro la variabile mattr*/
    rc=pthread_mutex_init(&buffer->mutex, &mattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_mutex_init  failed");

    /*Faccio uguale per la condition variabile, prima di tutto inizializzo l'attributo*/
    rc=pthread_condattr_init(&cvattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_condattr_init  failed");
    /*Imposto l'attributo per la condivisione*/
    rc=pthread_condattr_setpshared(&cvattr,PTHREAD_PROCESS_SHARED);
    if( rc ) PrintERROR_andExit(rc,"pthread_condattr_setpshared  failed");
    /*Inizializzo la condition variable cond_empty dentro la struttura*/
    rc=pthread_cond_init(&buffer->cond_empty, &cvattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_cond_init  failed");
    /*Faccio uguale anche per la condition variable cond_full*/
    rc=pthread_cond_init(&buffer->cond_full, &cvattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_cond_init  failed");

    /*Ora che sono finiti tutti i lavori preparatori che sono uguali sia per
     * il padre che per il figlio, avvio il processo figlio*/
    pid=fork();
    /*Divido i compiti a seconda se sono il figlio o il padre*/
    if(pid<0) { 
        PrintERROR_andExit(errno,"fork() failed ");
    }
    else if ( pid == 0) /* figlio */
        consumer_driver(buffer);
    else {  /* padre */
        producer_driver(buffer);
	/* sleep(15); */
	dezombizzaFiglio(pid);
    /*Quanto non ho più bisogno del file condiviso invio una richiesta di rimozione*/
	if (shm_unlink("/pedala") != 0) {
		perror("shm_unlink() failed");
		exit(1);
    	}
    }

    return(0);
}

void producer_driver(buffer_t *b) {
    int item;

    /*Stampo le istruzioni*/
    printf("digitare caratteri su tastiera\n");
    printf("digitare CTRL+D per terminare\n");
    fflush(stdout);

    while (1) {
        /*Salvo dentro item quello che l'utente ha scritto,
         * nota: la getchar mette solo caratteri, quindi se gli si passa
         * una stringa lui farà prima il primo carattere e poi quelli dopo
         * uno alla volta*/
        item = getchar();
        /*Controllo che non ci siano stati errori confrontandolo con la macro
         * EOF (end of file) che contiene un valore negativo che non può essere
         * assunto da nessun valore char, noi lo possiamo simulare con un CTRL+d
         * che è anche il comando per terminare il programma */
        if (item == EOF) {
            /*Metto nel buffer un carattere '\0' per terminare la stringa*/
            producer(b, '\0');
            /*Chiudo il processo del produttore, nota che quando uno tra il produttore
             * e il consumatore viene chiuso poi viene chiamata la funzione dezombizzaFiglio
             * che chiude tutto e quindi termina il programma*/
            break;
        } else { 
            /*Avvio una sola produzione e gli passo il valore che l'utente ha inserito
              (il while è qua non nella funzione producer)*/
            producer(b, (char)item);
        }
    }
}

void consumer_driver(buffer_t *b) {
    char item;/*int ris;*/

    while (1) {
        /*Avvio la funzione consumer e poi confronto se ha ritornato un '\0',
         * se così fosse saprei che l'utente ha digitato CTRL+d e quindi esco*/
        if ((item = consumer(b)) == '\0') {
            /*Se il consumer ritorna un '\0' allora significa che è stato producer a mettercelo
             * e quindi l'utente ha premuto CTRL+d e quindi fermo il ciclo e dopo
             * verrà chiamata la funzione dezombizzaFiglio per terminare correttamente il programma*/
            break;
        }
        /*ris=putchar(item); stampo a video il carattere */
        /*printf("\n"); La putchar non consente di mettere un \n alla fine della stampa*/

    /*Se ris è positivo allora è andato tutto bene altrimenti c'è stato un qualche errore nella putchar*/
	/*if(ris==EOF) PrintERROR_andExit(errno,"putchar failed");*/
    }
}




