#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>
int main(int argc, char* argv[]){
    printf("Lancio comando ls\n\n");
    execl("/bin/ls","ls", (char *)NULL);
    /*Questa stampa non verrà mai eseguita dato che execl rimpiazza
     * l'immagine del processo con una nuova che è appunto quella del ls.
     * Si può risolvere il problema lanciando da un processo figlio
     * la funzione execl e così facendo il figlio diventerà ls e il
     * processo principale potrà continuare.*/
    printf("Finito ls\n");
    return 0;
}
