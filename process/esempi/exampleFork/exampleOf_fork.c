#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>
int main(int argc, char* argv[]){
    int variabileLocale = 1;
    pid_t pid;
    /*int status; --> puntatore ad intero dove sarà contenuto 
     *                  il valore di ritorno del processo figlio*/

    /* crea un altro processo */
    printf("Padre: creo un figlio\n");
    pid = fork();

    /*Controllo se sono il figlio o il padre*/
    if (pid<0){
        fprintf(stderr,"Fork fallita.\n");
        exit(1);
    /*Nota: ho inserito una variabile locale così da mostrare che dall'inizio fino alla fork
     *      entrambi i processi hanno gli stessi dati*/
    } else if (pid==0){ /* processo figlio */
        printf("Figlio: avvio\n");
        sleep(5);
        printf("Figlio: variabileGlobale: %d\n", variabileLocale);
        printf("Figlio: finito\n");
    } else { /* processo genitore */
        wait(NULL); /*oppure waitpid(pid, &status, 0)*/
        printf("Figlio: variabileGlobale: %d\n", variabileLocale);
        printf("Padre: processo figlio terminato.\n");
        exit(0);
    }
}
