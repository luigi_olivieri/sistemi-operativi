#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<sys/mman.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>

#define SHARED_MEM_SIZE 512
#define SHARED_MEM_PATH "/test"

void startProcessoPadre();
void startProcessoFiglio();

int main(){
   pid_t pid;

    pid = fork();
    if (pid<0){
        perror("fork failed");
        exit(1);
    }
    else if(pid == 0){
        startProcessoFiglio();
    }
    else {
        startProcessoPadre();
    }

    return 0;
}

void startProcessoPadre(){
    char *shared_msg;
    int fileDescriptor, rc;
 
    fileDescriptor = shm_open(SHARED_MEM_PATH, O_CREAT | O_EXCL | O_RDWR, S_IRWXU | S_IRWXG);
    if (fileDescriptor < 0) {
        perror("shm_open() failed");
        exit(1);
    }
    
    rc = ftruncate(fileDescriptor, SHARED_MEM_SIZE);
    if (rc != 0) {
        perror("ftruncate() failed");
        exit(1);
    }

    shared_msg = (char *) mmap(NULL, SHARED_MEM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fileDescriptor, 0);
    if (shared_msg == MAP_FAILED) {
        perror("mmap() failed");
        exit(1);
    }

    memset(shared_msg, 66, SHARED_MEM_SIZE);
    printf("Il padre ha creato il segmento e ci ha scritto: \n%s", shared_msg);
}

void startProcessoFiglio(){

}
