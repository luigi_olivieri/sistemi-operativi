#include<stdio.h>
#include<pthread.h>
#include<unistd.h>
#include<inttypes.h>
#include"printerror.h"
#include"DBGpthread.h"

#define NUM_CAVERNICOLI 3
#define LATO_A 1
#define LATO_B -1

pthread_mutex_t mutex_glob;
pthread_cond_t cond_dinosauro;
pthread_cond_t cond_cavernicolo;

int pos_coda = LATO_A;
int pos_cavern[NUM_CAVERNICOLI] = {LATO_A, LATO_A, LATO_B};
int num_posti_coda = 2;
int flag_tutti_scesi = 0;

void *dinosauro(void *arg) {
    char DebugLabel[128] = "Dinosauro";
	while(1) {
		DBGpthread_mutex_lock(&mutex_glob, DebugLabel);
	
		/*Aspetto che siano saliti esattamente 2 passeggeri*/
		while (num_posti_coda != 0) {
			printf("Aspetto che siano saliti esattamente 2 passeggeri\n");
			DBGpthread_cond_wait(&cond_dinosauro, &mutex_glob, DebugLabel);
		}
		flag_tutti_scesi = 1;
		/*Sposto la coda impiegando 2 secondi*/
		printf("Sposto la coda impiegando 2 secondi\n");
		sleep(2);
		pos_coda = pos_coda * -1;	
		/*Avviso che siamo arrivati*/
		printf("Avviso che siamo arrivati in lato: %d\n", pos_coda);
		DBGpthread_cond_broadcast(&cond_cavernicolo, DebugLabel); 
		/*Aspetto che tutti siano scesi*/
		while (num_posti_coda < 2) {
			printf("Aspetto che tutti siano scesi\n");
			DBGpthread_cond_wait(&cond_dinosauro, &mutex_glob, DebugLabel);
		}
		/*Avviso i nuovi passeggeri che possono salire*/
		printf("Avviso i nuovi passeggeri che possono salire\n\n");
		flag_tutti_scesi = 0;
		DBGpthread_cond_broadcast(&cond_cavernicolo, DebugLabel);

		DBGpthread_mutex_unlock(&mutex_glob, DebugLabel);
	}

    pthread_exit(NULL);
}

void *cavernicolo(void *arg) {
	intptr_t myId = (intptr_t) arg;
    char DebugLabel[128];
    sprintf(DebugLabel,"Carvernicolo: %" PRIiPTR "" , myId);

	while(1) {
		DBGpthread_mutex_lock(&mutex_glob, DebugLabel);
		if (flag_tutti_scesi) {
			DBGpthread_cond_wait(&cond_cavernicolo, &mutex_glob, DebugLabel);
		}

		/*Se è dal mio lato e ce posto allora salgo*/
		if (pos_coda == pos_cavern[myId] && num_posti_coda > 0) {
			printf("Caver %"PRIiPTR" salito lato: %d\n", myId, pos_cavern[myId]);
			num_posti_coda--; /*Sono salito*/
			if (num_posti_coda == 0) {/*Sono l'ultimo che sale*/
				printf("Caver %"PRIiPTR", sono l'ultimo, risveglio il dino\n", myId);
				DBGpthread_cond_signal(&cond_dinosauro, DebugLabel);
			}
			printf("Io %"PRIiPTR" mi addormento fino all'arrivo\n", myId);
			DBGpthread_cond_wait(&cond_cavernicolo, &mutex_glob, DebugLabel); /*Attendo che il dinosauro mi trasporti*/
			printf("Io %"PRIiPTR" sono arrivato, scendo\n", myId);
			/*Siamo arrivati, il dino mi ha svegliato, ora mi trovo dall'altro lato*/
			pos_cavern[myId] = pos_cavern[myId] * -1;

			/*Mi addormento in attesa che scendano tutti*/
			num_posti_coda++;
			DBGpthread_cond_signal(&cond_dinosauro, DebugLabel);
			DBGpthread_cond_wait(&cond_cavernicolo, &mutex_glob, DebugLabel);
			/*Mi faccio un girettino di 4 secondi*/
    		DBGpthread_mutex_unlock(&mutex_glob, DebugLabel);
			sleep(4);
			printf("Io %"PRIiPTR" ho finito il giro, sono in: %d\n", myId, pos_cavern[myId]);
		} else {
			/*Tutti i posti sono occupati...aspetto il secondo giro*/
			/*printf("Io %"PRIiPTR" aspetto per il secondo giro\n", myId);*/
			DBGpthread_cond_wait(&cond_cavernicolo, &mutex_glob, DebugLabel);
		}
		DBGpthread_mutex_unlock(&mutex_glob, DebugLabel);
	}
	pthread_exit(NULL);
}

int main(void) {
	intptr_t id;
    int rc;
	pthread_t idThread;

    rc = pthread_mutex_init(&mutex_glob, NULL);
    if( rc ) PrintERROR_andExit(rc,"pthread_mutex_init failed");

	rc = pthread_cond_init(&cond_cavernicolo, NULL);
    if( rc ) PrintERROR_andExit(rc,"pthread_cond_init failed");

    rc = pthread_cond_init(&cond_dinosauro, NULL);
    if( rc ) PrintERROR_andExit(rc,"pthread_cond_init failed");

	rc = pthread_create(&idThread, NULL, dinosauro, NULL);
    if(rc) PrintERROR_andExit(rc,"pthread_create failed");

	for(id=0; id<NUM_CAVERNICOLI; id++) {
		rc = pthread_create(&idThread, NULL, cavernicolo, (void*)id);
        if(rc) PrintERROR_andExit(rc,"pthread_create failed");
	}
	
	pthread_exit(NULL);
}
