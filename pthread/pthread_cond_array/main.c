/* file:  CondVarSignal.c 
   Routine che fornisce un synchronization point. 
   E' chiamata da ognuno dei SYNC_MAX pthread, che si fermano 
   finche' tutti gli altri sono arrivati allo stesso punto di esecuzione. 
*/ 

/* simboli già messi nella riga di comando del compilatore 
#define _THREAD_SAFE
#define _REENTRANT
#define _POSIX_C_SOURCE 200112L
*/

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 
#include "printerror.h"

#define SYNC_MAX 5 

pthread_mutex_t  sync_lock;

/*Dichiaro array di condition variable*/
pthread_cond_t   sync_cond[SYNC_MAX]; 
int  sync_count = 0; 

void SyncPoint(pthread_t th) {
	int rc, index;

	/*Blocco per garantire l'integrità della sync_count*/ 
	rc = pthread_mutex_lock(&sync_lock); 
	if( rc ) PrintERROR_andExit(rc,"pthread_mutex_lock failed"); /* no EINTR */
    
    /*Salvo l'indice di arrivo di ciascun pthread*/
    index = sync_count;

    /*Incremento il counter di quelli arrivati*/
	sync_count++; 

	/*Controllo se deve aspettare o no*/
	if (sync_count < SYNC_MAX) {
		/*Metto in attesa i pthread con indentificativo l'ordine di entrata*/
		rc = pthread_cond_wait(&sync_cond[index], &sync_lock); 
		if(rc) PrintERROR_andExit(rc, "pthread_cond_wait failed");

        /*Quando ripartono lo segnalo*/
        printf("Sono %lu e sono uscito \n", th);

        /*Faccio ripartire anche il pthread che è entrato dopo di me*/
		rc = pthread_cond_signal(&sync_cond[index + 1]);
		if(rc) PrintERROR_andExit(rc,"pthread_cond_signal failed");
	} else {
		/*L'ultimo pthread entrato finirà qua, 
         * ora che ci siamo tutti faccio ripartire il primo arrivato*/
		rc = pthread_cond_signal(&sync_cond[0]);
		if(rc) PrintERROR_andExit(rc,"pthread_cond_signal failed");

        /*Metto in pausa l'ultimo pthread sennò uscirebbe prima degli altri*/
        rc = pthread_cond_wait(&sync_cond[index], &sync_lock);
		if(rc) PrintERROR_andExit(rc,"pthread_cond_signal failed");
        printf("Sono %lu e sono uscito \n", th);
    }

	/*Sblocco il mutex*/
	rc = pthread_mutex_unlock (&sync_lock); 
	if(rc) PrintERROR_andExit(rc,"pthread_mutex_unlock failed");
	return; 
} 

void *Thread (void *arg) 
{ 
	pthread_t  th; 

    /*Ritorna l'identificatore del thread*/
	th=pthread_self();
    printf ("%lu\n", th);

    SyncPoint(th);
	pthread_exit(NULL); 
} 

int main () 
{ 
	pthread_t th[SYNC_MAX]; 
	int  rc, n; intptr_t i;
	void *ptr; 

    /*Inizializzo l'array di condition variable*/
    for (n=0; n<SYNC_MAX; n++) {
    	rc = pthread_cond_init(&sync_cond[n], NULL);
	    if( rc ) PrintERROR_andExit(rc,"pthread_cond_init failed"); /* no EINTR */
    }
    
    /*Inizializzo la mutex variable*/
    rc = pthread_mutex_init(&sync_lock, NULL);
	if( rc ) PrintERROR_andExit(rc,"pthread_mutex_init failed"); /* no EINTR */

    /*Creo i pthread*/
	for(i=0;i<SYNC_MAX;i++) {
		rc = pthread_create(&(th[i]), NULL, Thread, NULL); 
		if (rc) PrintERROR_andExit(rc,"pthread_create failed"); /* no EINTR */
	}
    /*Attendo la fine di tutti i pthread*/
	for(i=0;i<SYNC_MAX;i++) {
		rc = pthread_join(th[i], &ptr ); 
		if (rc) PrintERROR_andExit(rc,"pthread_join failed"); /* no EINTR */
	}

    /*Elimino le variabili mutex e condition*/
	rc = pthread_mutex_destroy(&sync_lock); 
	if( rc ) PrintERROR_andExit(rc,"pthread_mutex_destroy failed"); /*no EINTR*/
	for (n=0; n<SYNC_MAX; n++) {
        rc = pthread_cond_destroy(&sync_cond[n]); 
    	if( rc ) PrintERROR_andExit(rc,"pthread_cond_destroy failed"); /*no EINTR*/
    }
	pthread_exit (NULL); 
} 
  
  
  
