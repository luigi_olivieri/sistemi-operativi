#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<unistd.h>

#define NUM_BUFFER 1
#define NUM_PROD 3
#define NUM_CONS 3

pthread_mutex_t GlobMutex;

pthread_cond_t  ProdConds;
pthread_cond_t  ConsConds;

int valBuffer;
int num_buffer_pieno = 0;
int num_prod_in_wait = 0;
int num_cons_in_wait = 0;
int num_prod_signaled = 0;
int num_cons_signaled = 0;

void *ThreadProd(void *arg) {
    while(1) {
        pthread_mutex_lock(&GlobMutex);
        /*Nel caso in cui il numero di quelli che vogliono svegliarsi
         * sia maggiore di quello che serve (numero dei buffer da riempire)
         * allora addormento questo produttore perché non serve*/
        if (num_prod_signaled >= (NUM_BUFFER - num_buffer_pieno)) {
            num_prod_in_wait++;
            pthread_cond_wait(&ProdConds, &GlobMutex);
            num_prod_in_wait--;
            num_prod_signaled--;
        }

        /*Inizio lavoro sul buffer
         * Simulo che ci voglia tempo per produrre il dato...*/
        sleep(1);
        valBuffer = rand();
        num_buffer_pieno++;
        printf("Inserito %d nel buffer\n", valBuffer);
        /*Fine lavoro sul buffer*/

        /*Sveglio un consumatore solo se ne ho che stanno dormendo e
         * solo se ho qualcosa da leggere nel buffer
         * Nota: si usa la variabile num_prod_signaled perché a me interessa non andare
         *       a svegliarne più di quelli che sono necessari
         *       (NON SI VUOLE SPRECARE UNA SIGNAL CHE NON SERVE) */

        if ( (num_cons_signaled < num_cons_in_wait) &&
             (num_cons_signaled < num_buffer_pieno) ){

            pthread_cond_signal(&ConsConds);
            num_cons_signaled++;
        }
        pthread_mutex_unlock(&GlobMutex);
    }
}

void *ThreadCons(void *arg) {
    while(1) {
        pthread_mutex_lock(&GlobMutex);
        /*Se ci sono troppi consumatori che vogliono svegliarsi
         * allora non è necessario svegliarli tutti per svuotare il buffer 
         * e quindi li addormento...*/
        if (num_cons_signaled >= num_buffer_pieno) {
            num_cons_in_wait++;
            pthread_cond_wait(&ConsConds, &GlobMutex);
            num_cons_in_wait--;
            num_cons_signaled--; /*La signal è arrivata, aggiorno il dato*/
        }

        /*Inizio lavoro sul buffer*
         * Simulo che ci voglia tempo a per consumare il dato */
        sleep(1);
        printf("Numero nel buffer: %d\n\n", valBuffer);
        num_buffer_pieno--;
        /*Fine lavoro sul buffer*/

        /*Sveglio i produttori solo se ce ne sono che stanno dormendo e solo se
         * c'è bisogno di un produttore per riempire il buffer.
         * Nota: si usa la variabile num_prod_signaled perché a me interessa non andare
         *       a svegliarne più di quelli che sono necessari 
         *       (NON SI VUOLE SPRECARE UNA SIGNAL CHE NON SERVE) */
        if ( (num_prod_signaled < num_prod_in_wait) && 
             (num_prod_signaled < (NUM_BUFFER - num_buffer_pieno)) ) {
            
            pthread_cond_signal(&ProdConds); /*Dico ad un produttore di iniziare a lavorare*/
            num_prod_signaled++; /*Segno che la signal è partita*/
        }
        pthread_mutex_unlock(&GlobMutex);
    }
}

int main(void) {
    pthread_t idThread;
    int i;

    pthread_mutex_init(&GlobMutex, NULL);
    pthread_cond_init(&ProdConds, NULL);
    pthread_cond_init(&ConsConds, NULL);

    for (i=0; i<NUM_PROD; i++) {
        pthread_create(&idThread, NULL, ThreadProd, NULL);
    }
    
    for (i=0; i<NUM_CONS; i++) {
        pthread_create(&idThread, NULL, ThreadCons, NULL);
    }

    pthread_exit(NULL);
}




