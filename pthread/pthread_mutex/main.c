/* Test per l'uso delle seguenti funzioni:
 * pthread_mutex
 * pthread_join (con valore di ritorno)
 * */

#include<stdio.h>
#include<pthread.h>
#define NUM_THREADS 10
pthread_t tid [NUM_THREADS ];
pthread_mutex_t mutexdata;
int data=4;
int threadReturnValue = 1;

void *func(void *arg);

int main (void) {
    int t;
    int *localThreadReturnValue;
    pthread_mutex_init(&mutexdata, NULL);
    /* creo i pthread */
    for(t=0;t < NUM_THREADS;t++) {
        pthread_create(&tid[t], NULL, func, NULL ) ;
    }
    /* attendo terminazione dei pthread */
    for(t=0;t < NUM_THREADS;t++) {
        /*Il valore di ritorno passato alla pthread_exit verrà scritto nella localThreadReturnValue*/
        pthread_join(tid[t], (void**) &localThreadReturnValue);
    }
    printf("data = %d\n", data);
    printf("p = %d\n", *localThreadReturnValue);
    pthread_mutex_destroy(&mutexdata);
    pthread_exit(NULL);
}

void *func( void *arg ) {
     pthread_mutex_lock(&mutexdata);
     if(data>0) {
        data--;
     }
     pthread_mutex_unlock(&mutexdata);
     pthread_exit(&threadReturnValue);
}
