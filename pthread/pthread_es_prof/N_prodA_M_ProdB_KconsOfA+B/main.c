#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<pthread.h>
#include<unistd.h>
#include<inttypes.h>

#define NUM_BUFFER_A 1
#define NUM_BUFFER_B 1
#define NUM_PROD_A 3
#define NUM_PROD_B 5
#define NUM_CONS 10

pthread_mutex_t GlobMutex;

pthread_cond_t  ProdCondsA;
pthread_cond_t  ProdCondsB;
pthread_cond_t  ConsConds;

uint64_t valBufferA;
uint64_t valBufferB;
int num_buffer_pieno_A = 0;
int num_buffer_pieno_B = 0;

void *ThreadProdA(void *arg) {
    while(1) {
        pthread_mutex_lock(&GlobMutex);

       /*Se il buffer di A ha spazio lo riempio, altrimenti addormento A*/ 
        while (num_buffer_pieno_A >= NUM_BUFFER_A) {
            pthread_cond_wait(&ProdCondsA, &GlobMutex);
        }
 
        /*Inizio lavoro sul buffer*/
        valBufferA = rand();
        printf("Il produttore A ha caricato %" PRIu64 " nel buffer\n", valBufferA);
        num_buffer_pieno_A++;
        /* Simulo che il produttore impieghi tempo a produrre*/
        sleep(1);
        /*Fine lavoro sul buffer*/

        /*Dico al consumatore che ho finito*/
        pthread_cond_signal(&ConsConds);
        pthread_mutex_unlock(&GlobMutex);
    }
}

void *ThreadProdB(void *arg) {
    while(1) {
        pthread_mutex_lock(&GlobMutex);

       /*Se il buffer di B ha spazio lo riempio, altrimenti addormento B*/ 
       while (num_buffer_pieno_B >= NUM_BUFFER_B) {
            pthread_cond_wait(&ProdCondsB, &GlobMutex);
        }
 
        /*Inizio lavoro sul buffer*/
        valBufferB = rand();
        printf("Il produttore B ha caricato %" PRIu64 " nel buffer\n", valBufferB);
        num_buffer_pieno_B++;
        /* Simulo che il produttore impieghi tempo a produrre*/
        sleep(1);
        /*Fine lavoro sul buffer*/

        /*Dico al consumatore che ho finito*/
        pthread_cond_signal(&ConsConds);
        pthread_mutex_unlock(&GlobMutex);
    }
}

void *ThreadCons(void *arg) {
    uint64_t tot;
    while(1) {
        pthread_mutex_lock(&GlobMutex);
        
        while( num_buffer_pieno_A <= 0 || num_buffer_pieno_B <= 0 ) {
            pthread_cond_wait(&ConsConds, &GlobMutex);
        }

        /*Inizio lavoro sul buffer*/
        tot = valBufferA + valBufferB;
        printf("Il consumatore ha caricato dal buffer: %" PRIu64 " + %" PRIu64" = %" PRIu64 "\n\n", 
                valBufferA, valBufferB, tot);
        fflush(stdout);
        num_buffer_pieno_A--;
        num_buffer_pieno_B--;
        /* Simulo che il produttore impieghi tempo a produrre*/
        sleep(1);
        /*Fine lavoro sul buffer*/

        /*Segnalo al produttore A che ora c'è un buffer da caricare*/
        pthread_cond_signal(&ProdCondsA);
        /*Segnalo al produttore B che ora c'è un buffer da caricare*/
        pthread_cond_signal(&ProdCondsB);
    
        pthread_mutex_unlock(&GlobMutex);
    }
}

int main(void) {
    pthread_t idThread;
    int i;

    pthread_mutex_init(&GlobMutex, NULL);
    pthread_cond_init(&ProdCondsA, NULL);
    pthread_cond_init(&ProdCondsB, NULL);
    pthread_cond_init(&ConsConds, NULL);

    for (i=0; i<NUM_PROD_A; i++) {
        pthread_create(&idThread, NULL, ThreadProdA, NULL);
    }
    for (i=0; i<NUM_PROD_B; i++) {
        pthread_create(&idThread, NULL, ThreadProdB, NULL);
    }
    for (i=0; i<NUM_CONS; i++) {
        pthread_create(&idThread, NULL, ThreadCons, NULL);
    }

    pthread_exit(NULL);
}

