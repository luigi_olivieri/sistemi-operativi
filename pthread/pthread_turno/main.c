#include <stdint.h>
#include<stdio.h>
#include<pthread.h>
#include<inttypes.h>
#include<unistd.h>
#define NUM_THREAD 3

pthread_mutex_t mutex_var;
pthread_cond_t  cond_var[NUM_THREAD];
int turno = 0;

void *thread(void *arg) {
    intptr_t index = (intptr_t) arg;

    while(1) {
        /*Blocco per garantire la variable globale turno*/
        pthread_mutex_lock(&mutex_var);

        /*Addormento il pthread se non è il suo turno
         * uso un while per una sicurezza massima
         * (anche if è funzionante)*/
        while(index != turno) {
            pthread_cond_wait(&cond_var[index], &mutex_var);
        }        

        /*Simulo una serie di operazioni svolte dal thread*/
        /*sleep(1);*/
        printf("Io thread °%" PRIiPTR " termino\n", index);
        
        /*Aumento il turno solo se non sono l'ultimo del gruppo*/
        if (turno < 2) {
            turno++;
        }
        /*Se sono l'ultimo della fila resetto il turno
         * così facendo il giro ricomincia*/
        else {
            turno = 0;
        }

        /*Finito di fare le mie operazioni, ora tocca a quello dopo*/
        pthread_cond_signal(&cond_var[turno]);

        pthread_mutex_unlock(&mutex_var);
    }
    pthread_exit(NULL);
}

int main(void) {
    int i;
    intptr_t index;
    pthread_t pthread_id;

    /*Inizializzo la mutex variable*/
    pthread_mutex_init(&mutex_var, NULL);

    /*Inizializzo le condition variable*/
    for (i=0; i<NUM_THREAD; i++) {
        pthread_cond_init(&cond_var[i], NULL);
    }

    /*Creo i pthread*/
    for (i=0; i<NUM_THREAD; i++) {
        index = i;
        pthread_create(&pthread_id, NULL, thread, (void*) index);
    } 

    pthread_exit(NULL);
}
