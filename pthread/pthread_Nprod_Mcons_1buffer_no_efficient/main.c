#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<unistd.h>

#define NUM_BUFFER 1
#define NUM_PROD 3
#define NUM_CONS 3

pthread_mutex_t GlobMutex;

pthread_cond_t  ProdConds;
pthread_cond_t  ConsConds;

int valBuffer;
int num_buffer_pieno = 0;

void *ThreadProd(void *arg) {
    while(1) {
        pthread_mutex_lock(&GlobMutex);
 
        /*Se i buffer da riempire non ci sono e quindi
         * il numero di buffer pieni è pari o addirittura maggiore
         * del numero di buffer totali (è possibile perché siamo in
         * un contesto di parallelismo) allora addormento questo
         * produttore perché non c'è nulla da riempire */
        while (num_buffer_pieno >= NUM_BUFFER) {
            pthread_cond_wait(&ProdConds, &GlobMutex);
        }
 
        /*Inizio lavoro sul buffer*/
        valBuffer = rand();
        printf("Il produttore ha caricato %d nel buffer\n", valBuffer);
        num_buffer_pieno++;
        /* Simulo che il produttore impieghi tempo a produrre*/
        sleep(1);
        /*Fine lavoro sul buffer*/

        /*Finito il lavoro del produttore avviso un consumatore*/
        pthread_cond_signal(&ConsConds);
        pthread_mutex_unlock(&GlobMutex);
    }
}

void *ThreadCons(void *arg) {
    while(1) {
        pthread_mutex_lock(&GlobMutex);
        
        /*Se il numero dei buffer pieni è 0 o meno 
         * (possibile perché ci troviamo in un contesto di parallelismo)
         * allora non mi serve svuotare nulla e quindi addormento
         * il consumatore */
        while(num_buffer_pieno <= 0 ) {
            pthread_cond_wait(&ConsConds, &GlobMutex);
        }

        /*Inizio lavoro sul buffer*/
        printf("Il consumatore ha caricato dal buffer: %d\n\n", valBuffer);
        fflush(stdout);
        num_buffer_pieno--;
        /* Simulo che il consumatore impieghi tempo a consumare*/
        sleep(1);
        /*Fine lavoro sul buffer*/

        /*Segnalo ad un produttore che ora c'è un buffer da caricare*/
        pthread_cond_signal(&ProdConds);
        pthread_mutex_unlock(&GlobMutex);
    }
}

int main(void) {
    pthread_t idThread;
    int i;

    pthread_mutex_init(&GlobMutex, NULL);
    pthread_cond_init(&ProdConds, NULL);
    pthread_cond_init(&ConsConds, NULL);

    for (i=0; i<NUM_PROD; i++) {
        pthread_create(&idThread, NULL, ThreadProd, NULL);
    }
    
    for (i=0; i<NUM_CONS; i++) {
        pthread_create(&idThread, NULL, ThreadCons, NULL);
    }

    pthread_exit(NULL);
}




