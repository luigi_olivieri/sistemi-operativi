/* Test per verificare il funzionamento del tipo di dato
 * int grande come un puntatore ad int chiamato intptr_t,
 * con questo non sono necessari cast a puntatore e sopratutto
 * non è necessario allocare il valore da passare al thread
 */

#include<stdio.h>
#include<stdint.h>
#include<pthread.h>
#include<inttypes.h>
#define NUM_THREADS 10

void *func(void *arg); 

int main(void) {
    pthread_t tid;
    intptr_t t;

    for(t=0;t < NUM_THREADS; t++) {
        pthread_create(&tid, NULL, func, (void *) t);
    }

    pthread_exit(NULL);
}

void *func(void *arg) {
    intptr_t indice;
    indice = ((intptr_t) arg);
    /* NON DEVO rilasciare memoria
     NOOO!!!!!!! free( arg );
    */
    printf("Ho ricevuto %" PRIiPTR " \n", indice);
    pthread_exit(NULL);
}
