#include<stdio.h>
#include<pthread.h>
#include<inttypes.h>
#include<stdint.h>
#define NUM_SPADE 10

pthread_mutex_t mutex_arr[NUM_SPADE];

void *es_fachiri(void *arg) {
    int i;
    intptr_t id = (intptr_t) arg;

    while (1) {
        /*Primo ciclo blocca tutte le mutex contenute nella mutex_arr*/
        for (i=0; i<NUM_SPADE; i++) {
            printf("Io fachiro n°%" PRIiPTR " prendo la spada n°%d\n", id, i);
            pthread_mutex_lock(&mutex_arr[i]);
        }

        /*Immagina che in questo punto del codice io utilizzi delle variabili
         * che ho dovuto bloccare con le mutex
         * (L'esercizio di ghini chiedeva di urlare non di usare le mutex...:) )*/
        printf("Ahia, dio bellooooo\n");
        fflush(stdout);

        /*Il secondo ciclo libera tutte le mutex perché non le utilizzo più*/
        for (i=0; i<NUM_SPADE; i++) {
            printf("Io fachiro n°%" PRIiPTR " ripongo la spada n°%d\n", id, i);
            pthread_mutex_unlock(&mutex_arr[i]);
        }
    }
     
    pthread_exit(NULL);
}

int main(void) {
    int i;
    intptr_t id; /*Necessario per passare un argomento al thread senza allocare*/
    /*Creato perché costretto, la create lo esige anche se non lo uso*/
    pthread_t thread_id; 
    
    /*Inizializzo le mutex dentro l'array*/
    for (i=0; i<NUM_SPADE; i++) {
        pthread_mutex_init(&mutex_arr[i], NULL);
    }

    /*Creo i pthread passando come argomento un id*/
    id = 1;
    pthread_create(&thread_id, NULL, es_fachiri, (void*)id);
    id = 2;
    pthread_create(&thread_id, NULL, es_fachiri, (void*)id);

    pthread_exit(NULL);
}
