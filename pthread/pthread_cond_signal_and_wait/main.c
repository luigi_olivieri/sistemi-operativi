/*
 * Test per uso delle funzioni:
 * pthread_cond_signal
 * pthread_cond_wait
 * 
 * In pratica la signal manda il segnale di sblocco e la wait
 * rende il thread dormiente in attesa del risveglio della signal
 *
 * Nota: questo programma è da visionare in codice ma non lo avviare
 *       in quanto sarebbe un loop infinito dato che la gallina produce
 *       all'infinito e la volpe mangia all'infinito...
 */

#include<stdio.h>
#include<pthread.h>
#include<unistd.h>

int uova = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void *gallina_produce_uova(void *arg)
{ 
    while (1) { /* gallina produce uova impiegando tempo ....*/
        pthread_mutex_lock(&mutex);
        uova ++;
        printf("Appena prodotto un uovo, uova totali: %d\n", uova);
        pthread_cond_signal(&cond); /* avvisa volpe che c'è uovo, e continua */
        pthread_mutex_unlock(&mutex);
    }
}

void *volpe_mangia_uova(void *arg)
{ 
    while (1) { 
        int mangiato=0;
        pthread_mutex_lock(&mutex);
    
        while (!mangiato) {
            if (uova > 0) {
                /* Momento in cui volpe mangia uovo impiegando tempo ....*/ 
                uova --;
                printf("Uovo mangiato, uova rimaste: %d\n", uova);
                mangiato=1;
            } else {
                /* attendi che venga prodotto uovo */
                printf("Attendo che venga prodotto un uovo...\n");
                pthread_cond_wait(&cond, &mutex);
            }
        }
        pthread_mutex_unlock(&mutex);
        /* Momento in cui la volpe digerisce impiegando tempo */
    }
}

int main(void)
{ 
    pthread_t tid;
    pthread_create(&tid, NULL, gallina_produce_uova, NULL);
    pthread_create(&tid, NULL, volpe_mangia_uova, NULL);
    /* Nota: finito l'uso delle condition variable, bisognerebbe rilasciarle...
     *       ma dato che questo programma è un loop infinito non serve...
     * Per rilasciarle usa:
            pthread_cond_destroy(&mycond);
     */
    pthread_exit(NULL);
}
